#include "stdafx.h"
#include "PaysysSDZhupf.h"

PaysysSDZhupf* g_pPaysysSDZhupf;
DWORD *WINAPI netproc(int a2, int a3);
DWORD WINAPI Init(LPVOID pParam)
{
	LOGFILE("PaysysSDZhupf Init");
	::MessageBoxA(NULL, "Paysys Init", "Paysys Test", MB_OK);
	g_pPaysysSDZhupf = new PaysysSDZhupf();
	return 0;
}

PaysysSDZhupf::PaysysSDZhupf()
	: m_kULHook((PROC)0x404980, (PROC)netproc)
{
}


PaysysSDZhupf::~PaysysSDZhupf()
{
}


//DWORD* WINAPI netproc_404980(void *this_, int a2, int a3)
//{
//	return NULL;
//}

typedef DWORD* (__cdecl *pfn_newPack)(int a1);
pfn_newPack newPack = (pfn_newPack)0x402F80;
typedef void(__fastcall *pfn_sendPack)(void * this_ptr, int dummy, int, DWORD *);
pfn_sendPack sendPack = (pfn_sendPack)0x404F70;
DWORD *WINAPI netproc(int a2, int this_ptr)
{
	DWORD *proc; // eax@1
	int v4; // ST40_4@4
	int v5; // eax@8
	int v6; // eax@14
	int v7; // ST28_4@14
	int v8; // ST1C_4@23
	int v9; // eax@27
	int v10; // ST10_4@27
	DWORD *v11; // [sp+0h] [bp-44h]@1
	DWORD *v12; // [sp+Ch] [bp-38h]@26
	DWORD *v13; // [sp+18h] [bp-2Ch]@22
	DWORD *v14; // [sp+20h] [bp-24h]@13
	DWORD *v15; // [sp+30h] [bp-14h]@7
	DWORD *v16; // [sp+38h] [bp-Ch]@3
	DWORD *v17; // [sp+40h] [bp-4h]@1
	DWORD *this_ = (DWORD *)this_ptr;
	v11 = this_;
	proc = (DWORD *)(*(int(__thiscall **)(int))(*(DWORD *)this_ptr + 12))(this_ptr);
	v17 = proc;
	//     112 B2P_PING
	if (*(BYTE *)proc != 112)
	{
		switch (*(BYTE *)proc)
		{
		case 0x24: //36 B2P_BISHOP_IDENTITY_VERIFY
			v16 = newPack(51);
			if (v16)
			{
				v4 = (*(int(__thiscall **)(DWORD *))(*v16 + 12))(v16);
				*(BYTE *)v4 = 36;
				*(WORD *)(v4 + 1) = 50;
				*(WORD *)(v4 + 5) = 4;
				memcpy((void *)(v4 + 11), (char *)v17 + 11, 0x20u);
				*(DWORD *)(v4 + 43) = 1;
				*(DWORD *)(v4 + 47) = GetTickCount();
				sendPack(v11, 0, a2, v16);
				//(*(void(__thiscall **)(void *, int, DWORD *))(*(DWORD *)v11 + 16))(v11, a2, v16);
			}
			proc = (DWORD *)printf("Bishop[dwUserIP:%u]��½\n", *(DWORD *)((char *)v17 + 111));
			break;
		case 0x4F://79 B2P_GET_ZONE_CHARGE_FLAG
			proc = newPack(179);
			v15 = proc;
			if (proc)
			{
				v5 = (*(int(__thiscall **)(DWORD *))(*proc + 12))(proc);
				*(BYTE *)v5 = 79;
				*(WORD *)(v5 + 1) = 178;
				*(WORD *)(v5 + 5) = 66;
				memcpy((void *)(v5 + 11), (char *)v17 + 11, 0x20u);
				sendPack(v11, 0, a2, v15);
				//proc = (DWORD *)(*(int(__thiscall **)(void *, int, DWORD *))(*(DWORD *)v11 + 16))(v11, a2, v15);
			}
			break;
		case 0x27: //39 B2P_BISHOP_LOGOUT
			proc = (DWORD *)printf("Bishop�˳�\n");
			break;
		case 0x25: //37 B2P_BISHOP_RECONNECT_IDENTITY_VERIFY
			proc = newPack(51);
			v14 = proc;
			if (proc)
			{
				v6 = (*(int(__thiscall **)(DWORD *))(*proc + 12))(proc);
				v7 = v6;
				*(BYTE *)v6 = 36;
				*(WORD *)(v6 + 1) = 50;
				*(WORD *)(v6 + 5) = 4;
				memcpy((void *)(v6 + 11), (char *)v17 + 11, 0x20u);
				*(DWORD *)(v7 + 43) = 1;
				*(DWORD *)(v7 + 47) = GetTickCount();
				sendPack(v11, 0, a2, v14);
				//proc = (DWORD *)(*(int(__thiscall **)(void *, int, DWORD *))(*(DWORD *)v11 + 16))(v11, a2, v14);
			}
			break;
		default:
			//0x21 33 B2P_PLAYER_IDENTITY_VERIFY
			//0x22 34 B2P_PLAYER_ENTER_GAME
			if (*(BYTE *)proc != 0x21 && *(BYTE *)proc != 34)
			{
				proc = (DWORD *)*(BYTE *)proc;
				//0x23 35 B2P_PLAYER_LEAVE_GAME
				//     40 B2P_EXT_POINTS_OPERATION
				if (proc != (DWORD *)35 && *(BYTE *)v17 != 40)
				{
					proc = v17;
					//     112 B2P_PING
					if (*(BYTE *)v17 != 112)
					{
						proc = (DWORD *)*(BYTE *)v17;
						//     62 B2P_IB_PLAYER_IDENTITY_VERIFY
						if (proc == (DWORD *)62)       // login
						{
							proc = newPack(0xA7);
							v13 = proc;
							if (proc)
							{
								v8 = (*(int(__thiscall **)(DWORD))(*proc + 12))((DWORD)proc);
								memset((void *)(v8 + 1), 0, 0xA6u);
								*(BYTE *)v8 = 62;
								*(WORD *)(v8 + 1) = 166;
								*(WORD *)(v8 + 5) = 9;
								*(DWORD *)(v8 + 7) = *(DWORD *)((char *)v17 + 7);
								*(WORD *)(v8 + 3) = *(WORD *)((char *)v17 + 3);
								*(DWORD *)(v8 + 43) = 1;
								strcpy((char *)(v8 + 11), (const char *)v17 + 11);
								*(DWORD *)(v8 + 91) = *(DWORD *)((char *)v17 + 107);
								*(DWORD *)(v8 + 143) = 1;
								*(DWORD *)(v8 + 87) = GetTickCount();
								*(DWORD *)(v8 + 119) = 1;
								*(DWORD *)(v8 + 103) = 0;
								*(DWORD *)(v8 + 95) = 1000000;
								*(BYTE *)(v8 + 148) = 0;
								*(BYTE *)(v8 + 147) = 0;
								memcpy((void *)(v8 + 149), "123456", 6u);
								sendPack(v11, 0, a2, v13);
								//proc = (DWORD *)(*(int(__thiscall **)(DWORD, DWORD, DWORD *))(*(DWORD *)v11 + 16))((DWORD)v11, a2, v13);
							}
						}
						//     60 B2P_IB_PLAYER_BUY_ITEM
						else if (*(BYTE *)v17 == 60)
						{
							proc = newPack(75);
							v12 = proc;
							if (proc)
							{
								v9 = (*(int(__thiscall **)(DWORD))(*proc + 12))((DWORD)proc);
								v10 = v9;
								*(BYTE *)v9 = 60;
								*(WORD *)(v9 + 1) = 74;
								*(WORD *)(v9 + 5) = 31;
								*(DWORD *)(v9 + 7) = *(DWORD *)((char *)v17 + 7);
								*(WORD *)(v9 + 3) = *(WORD *)((char *)v17 + 3);
								strcpy((char *)(v9 + 11), (const char *)v17 + 11);
								*(DWORD *)(v10 + 43) = *(DWORD *)((char *)v17 + 43);
								*(DWORD *)(v10 + 47) = *(DWORD *)((char *)v17 + 47);
								*(DWORD *)(v10 + 51) = *(DWORD *)((char *)v17 + 51);
								*(DWORD *)(v10 + 55) = *(DWORD *)((char *)v17 + 55);
								*(DWORD *)(v10 + 59) = *(DWORD *)((char *)v17 + 63);
								*(INT64 *)(v10 + 63) = rand() % 0xFFFFFFFF + 74578267812i64;
								*(DWORD *)(v10 + 71) = 1;
								sendPack(v11, 0, a2, v12);
								//proc = (DWORD *)(*(int(__thiscall **)(DWORD, DWORD, DWORD *))(*(DWORD *)v11 + 16))((DWORD)v11, a2, v12);
							}
						}
					}
				}
			}
			break;
		}
	}
	return proc;
}